// ������� 20
// ��� ������, ������ ������� �������� �������� ���� �����. ��������� �������
// find_if ����� ������ �����, ������������ � ������� �����.

#include "stdafx.h"
#include <list>
#include <string>
#include <algorithm>
#include <iostream>

class FindInList
{
public:

	FindInList(const std::string& letterToFind): _lettersToFind(letterToFind)
	{}

	bool operator() (const std::string& element)
	{
		if (element.empty())
			return false;
		if (find(_lettersToFind.begin(), _lettersToFind.end(), element[0]) != _lettersToFind.end())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

private:
	const std::string& _lettersToFind;
};

template <class T>
class FindInListTemplated
{
public:
	FindInListTemplated(const T& arrayOfElementsToFind) : _arrayOfElementsToFind(arrayOfElementsToFind)
	{}

	bool operator() (const T& element)
	{
		if (element.empty())
			return false;
		if (find(_arrayOfElementsToFind.begin(), _arrayOfElementsToFind.end(), element[0]) != _arrayOfElementsToFind.end())
		{
			return true;
		}
		else
		{
			return false;
		}
	}

private:
	const T& _arrayOfElementsToFind;
};

int main()
{
	std::list<std::string> myWordList = {"hello", "world", "do", "you", "like", "to", "code?", "of", "course!", "me", "too!"};
	std::string lettersToFind("aeiou");

	// ������� 1
	auto foundWord = find_if(myWordList.begin(), myWordList.end(), [&](std::string element) {
		if (find(lettersToFind.begin(), lettersToFind.end(), element[0]) != lettersToFind.end())
			return true;
		return false;
	});
	if (foundWord != myWordList.end())
	{
		std::cout << *foundWord << std::endl;
	}
	else
	{
		std::cout << "I can't find such a word in a list. Sorry" << std::endl;
	}

	// ������� 2
	foundWord = find_if(myWordList.begin(), myWordList.end(), FindInList(lettersToFind));
	if (foundWord != myWordList.end())
	{
		std::cout << *foundWord << std::endl;
	}
	else
	{
		std::cout << "I can't find such a word in a list. Sorry" << std::endl;
	}

	// ������� 3
	foundWord = find_if(myWordList.begin(), myWordList.end(), FindInListTemplated<std::string>(lettersToFind));
	if (foundWord != myWordList.end())
	{
		std::cout << *foundWord << std::endl;
	}
	else
	{
		std::cout << "I can't find such a word in a list. Sorry" << std::endl;
	}

	return 0;
}
